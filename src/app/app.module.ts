import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from "@angular/http";
import { IonicStorageModule } from '@ionic/storage';

import { MyApp } from './app.component';
import { LoginPage } from '../pages/login/login';
import {MainMenuPage} from '../pages/main-menu/main-menu';
import { ProductsPage } from '../pages/products/products';
import { ContactPage } from '../pages/contact/contact';
import { TimeoutsPage } from '../pages/timeouts/timeouts';
import { OrderPage } from '../pages/order/order';
import { ProductPage } from '../pages/product/product';
import { GetProductsService } from '../products-service/get-products.service';



@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    MainMenuPage,
    TimeoutsPage,
    ContactPage,
    ProductsPage,
    OrderPage,
    ProductPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot({
      name: '__mydb',
      driverOrder: ['indexeddb', 'sqlite', 'websql']
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    MainMenuPage,
    TimeoutsPage,
    ContactPage,
    ProductsPage,
    OrderPage,
    ProductPage
  ],
  providers: [
    StatusBar,
    GetProductsService,
    SplashScreen,
    IonicStorageModule,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
