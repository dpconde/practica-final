import {CartItem} from "./CartItem";

export class Cart {
  username: string;
  items: CartItem[];
  total: number;

  constructor(username) {
    this.username = username;
    this.items = [];
  }
}
