export class CartItem {

  id: number;
  quantity:number;
  product: Object;
  subtotal:number;

  constructor(id, quantity,product) {
    this.id = id;
    this.quantity = quantity;
    this.product = product;
  }
}
