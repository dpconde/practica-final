export class Product {

  id: number;
  product_image: string;
  product_name: string;
  product_dosage: string;
  product_price: number;
  product_distributor: string;
  distributor_address: string;
  distributor_email: string;
  distributor_phone: string;
  product_diagnosis: string;


  constructor(object:any) {
    this.id = object.id;
    this.product_image = object.product_image;
    this.product_name = object.product_name;
    this.product_dosage = object.product_dosage;

    //convert price
    if(object.product_price.startsWith('0')){
      object.product_price = object.product_price.substr(0, 1) + ',' + object.product_price.substr(1);
    }
    object.product_price = object.product_price.replace(/,/g, '.');
    object.product_price = object.product_price.replace('€', '');

    this.product_price = +object.product_price;
    this.product_distributor = object.product_distributor;
    this.distributor_address = object.distributor_address;
    this.distributor_email = object.distributor_email;
    this.distributor_phone = object.distributor_phone;
    this.product_diagnosis = object.product_diagnosis;
  }
}
