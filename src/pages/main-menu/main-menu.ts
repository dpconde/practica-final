import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {LoginPage} from '../login/login';
import {ProductsPage} from '../products/products';
import {TimeoutsPage} from '../timeouts/timeouts';
import {ContactPage} from '../contact/contact';
import { OrderPage } from '../order/order';
import { Storage } from '@ionic/storage';
import {Cart} from "../../model/Cart";


/**
 * Generated class for the MainMenuPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-main-menu',
  templateUrl: 'main-menu.html',
})
export class MainMenuPage {

  cartTotalQuantity: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  }

  ionViewDidLoad() {
  }

  ionViewWillEnter() {
    this.setBadgeQuantity();
  }

  /**
   * Function called when 'products' button is clicked
   */
  goToProducts(){
    this.navCtrl.push(ProductsPage);
  }

  /**
   * Function called when 'timeouts' button is clicked
   */
  goToTimeOuts(){
    this.navCtrl.push(TimeoutsPage);
  }

  /**
   * Function called when 'contact' button is clicked
   */
  goToContact(){
    this.navCtrl.push(ContactPage);
  }

  /**
   * Function called when 'cart icon' button is clicked
   */
  goToOrder(){
    this.navCtrl.push(OrderPage);
  }

  /**
   * Function called when logout button is clicked
   */
  logOut(){
    this.navCtrl.setRoot(LoginPage);
  }

  /**
   * Set Cart quantity
   */
  setBadgeQuantity(){
    let sum = 0;
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            sum = sum + item.quantity;
          }
          this.cartTotalQuantity = sum;
        }
      });
    });
  }
}
