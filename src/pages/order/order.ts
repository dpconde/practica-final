import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import {Cart} from "../../model/Cart";

/**
 * Generated class for the OrderPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {

  cartItems: any = [];
  totalPrice: number = 0;

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
    this.loadPage();
  }

  ionViewDidLoad() {
  }

  /**
   * Load cart items
   */
  loadPage(){
    let totalPrice = 0;
    let cartItems = [];
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            cartItems.push(item);
            totalPrice = totalPrice + (item.quantity * item.product.product_price)
          }
          this.cartItems = cartItems;
          this.totalPrice = totalPrice;
        }
      });
    });
  }

  /**
   * Add quantity to item cart
   * @param cartItem
   */
  addQuantity(cartItem){
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            if(item.id == cartItem.id){
              item.quantity = item.quantity + 1;
              break;
            }
          }
          this.storage.set(name+'#cart',cart);
          this.loadPage();
        }
      });
    });
  }

  /**
   * Remove quantity from item cart
   * @param cartItem
   */
  removeQuantity(cartItem){
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            if(item.id == cartItem.id){
              if(item.quantity==1){
                this.deleteItem(cartItem);
              }else{
                item.quantity = item.quantity - 1;
              }
              break;
            }
          }
          this.storage.set(name+'#cart',cart);
          this.loadPage();
        }
      });
    });
  }

  /**
   * Delete item from cart
   * @param cartItem
   */
  deleteItem(cartItem){
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          var index = 0;
           for(let item of cart.items){
            index++;
            if(item.id == cartItem.id){
              cart.items.splice(index-1, 1);
              break;
            }
          }
          this.storage.set(name+'#cart',cart);
          this.loadPage();
        }
      });
    });
  }

}
