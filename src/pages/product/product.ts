import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { GetProductsService } from '../../products-service/get-products.service';
import { Storage } from '@ionic/storage';
import { OrderPage } from '../order/order';
import {Cart} from "../../model/Cart";
import {CartItem} from "../../model/CartItem";
import {Product} from "../../model/Product";
import {AlertController} from 'ionic-angular';


/**
 * Generated class for the ProductPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-product',
  templateUrl: 'product.html',
})



export class ProductPage {

  product: Product = <any>{};
  quantity: number = 1;
  cartTotalQuantity: number = 0;
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(public navCtrl: NavController, public navParams: NavParams, private alertController: AlertController,
              private getProductsService: GetProductsService, public loadingCtrl: LoadingController,private storage: Storage) {
    let id = navParams.get('id');
    this.requestProduct(id);
  }

  ionViewWillEnter() {
    this.setBadgeQuantity();
  }

  /**
   * Request product by Id from server
   * @param id
   */
  requestProduct(id:number){
    this.loading.present();
    this.getProductsService.getProductById(id)

      .then(data => {
        const response: any = data;

        if (response.status === 'KO') {
          this.loading.dismiss();
          this.alertController.create({
            title: 'Error',
            subTitle: 'Unable to retrieve product',
            buttons: ['OK']
          }).present();
        }else{
          this.loading.dismiss();
          this.product = new Product(data);
        }
      })

      .catch(error => {
        this.loading.dismiss();
        this.alertController.create({
          title: 'Error',
          subTitle: 'Unable to retrieve product',
          buttons: ['OK']
        }).present();
      });
  }

  /**
   * Increase product quantity
   */
  addQuantity(){
    this.quantity = this.quantity + 1;
  }

  /**
   * Remove product quantity
   */
  removeQuantity(){
    if(this.quantity>1){
      this.quantity = this.quantity - 1;
    }
  }

  /**
   * Add product to current cart
   * @param idProduct
   * @param product
   */
  addToCart(idProduct:number, product:Product){
    var cartItem = new CartItem(idProduct, this.quantity, product);

    this.storage.get('userName').then((name) => { //get current user
      this.storage.get(name+'#cart').then((cart) => { //get user cart

        if(cart==null){
          cart = new Cart(name);
          cart.items.push(cartItem);
        }else{
          cart = cart as Cart;
          var existsItem: Boolean = false;
          for(let item  of cart.items){
            if(item.id == product.id){
                item.quantity = item.quantity + this.quantity;
                existsItem = true;
                break;
            }
          }
          if(existsItem == false){
            cart.items.push(cartItem);
          }
        }
        this.storage.set(name+'#cart', cart);
        this.setBadgeQuantity();
      });
    });
  }

  /**
   * Open cart detail. Called when 'cart icon' is clicked
   */
  goToOrder(){
    this.navCtrl.push(OrderPage);
  }

  /**
   * Set Cart quantity
   */
  setBadgeQuantity(){
    let sum = 0;
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            sum = sum + item.quantity;
          }
          this.cartTotalQuantity = sum;
        }
      });
    });
  }

}
