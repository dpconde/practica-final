import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { Http, Headers } from '@angular/http';
import {MainMenuPage} from '../main-menu/main-menu';
import {AlertController} from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the LoginPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username: string = '';
  password: string = '';
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });


  constructor(public navCtrl: NavController, public loadingCtrl: LoadingController, public navParams: NavParams, public http: Http,
              private alertController: AlertController, private storage: Storage) {
  }

  /**
   * Login to server
   * @param username
   * @param password
   */
  doLogin(username,password) {
    this.loading.present();
    const headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
    const body = 'user=' + username + '&passwd=' + password;

    this.http.post('http://multimedia.uoc.edu/frontend/auth.php', body, {headers: headers})
      .subscribe(data => {
        var response = JSON.parse(data['_body']);

        if(response.status=="OK"){ //Login OK
          this.loading.dismiss();
          this.storage.set('userName',username);
          this.navCtrl.setRoot(MainMenuPage);

        }else{//Login KO
          this.loading.dismiss();
            this.alertController.create({
              title: 'Invalid credentials',
              subTitle: 'Please check your username and password.',
              buttons: ['OK']
            }).present();

        }

      }, error => {
        console.log(error);// Error getting the data
        alert(error);
      });
  }

  /**
   * Submit form
   */
  submit(){
    this.doLogin(this.username,this.password);
  }

  ionViewDidLoad() {
  }
}
