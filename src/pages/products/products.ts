import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, LoadingController} from 'ionic-angular';
import { OrderPage } from '../order/order';
import { ProductPage } from '../product/product';
import { Http } from '@angular/http';
import { GetProductsService } from '../../products-service/get-products.service';
import {AlertController} from 'ionic-angular';


import { Storage } from '@ionic/storage';
import {Cart} from "../../model/Cart";



/**
 * Generated class for the ProductsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-products',
  templateUrl: 'products.html',
})
export class ProductsPage {

  products: any = [];
  page: number = 1;
  cartTotalQuantity: number = 0;
  loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(public navCtrl: NavController, private getProductsService: GetProductsService, private alertController: AlertController,
              public navParams: NavParams, public http: Http, public loadingCtrl: LoadingController,private storage: Storage) {
    this.requestProducts(true);
  }

  /**
   * Open cart detail. Called when 'cart icon' is clicked
   */
  goToOrder(){
    this.navCtrl.push(OrderPage);
  }

  /**
   * Open product detail. Called when View button is clicked
   * @param product
   */
  goToDetail(product){
    this.navCtrl.push(ProductPage, {
      id: product.id
    });
  }


  ionViewWillEnter() {
    this.setBadgeQuantity();
  }


  /**
   * Retrieve products from server
   * @param showLoading
   */
  requestProducts(showLoading) {
    if(showLoading){this.loading.present()};
    this.getProductsService.getProductsByPage(this.page)

        .then(data => {
          const response: any = data;
          if (response.status === 'KO') {
            if(showLoading){this.loading.dismiss()};
            this.alertController.create({
              title: 'Error',
              subTitle: 'Unable to retrieve products',
              buttons: ['OK']
            }).present();
          } else {
            if(showLoading){this.loading.dismiss()};
            this.products = this.products.concat(data);
            this.page = this.page + 1;
          }
        })
        .catch(error => {
          if(showLoading){this.loading.dismiss()};
          this.alertController.create({
            title: 'Error',
            subTitle: 'Unable to retrieve products',
            buttons: ['OK']
          }).present();
        });
  }

  /**
   * Infinite Scroll implementation
   * @param infiniteScroll
   */
  doInfinite(infiniteScroll) {
    setTimeout(() => {
      this.requestProducts(false);

      infiniteScroll.complete();
    }, 500);
  }


  /**
   * Set Cart quantity
   */
  setBadgeQuantity(){
    let sum = 0;
    this.storage.get('userName').then((name) => {
      this.storage.get(name+'#cart').then((cart) => {
        if(cart!=null){
          cart as Cart
          for(let item of cart.items){
            sum = sum + item.quantity;
          }
          this.cartTotalQuantity = sum;
        }
      });
    });
  }


}
