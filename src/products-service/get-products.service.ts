import { Injectable } from '@angular/core';

import { Http } from '@angular/http';

import 'rxjs/add/operator/map';


@Injectable()
export class GetProductsService {

  constructor( private http: Http) { }

  public getProductsByPage(page: number) {
    // In asynchronous requests it's recommended the use of Promises or Observables
    // (in our cases as we have a unique response a Promise will be enough)
    return new Promise(
      resolve => {
        this.http.get('http://multimedia.uoc.edu/frontend/getproducts.php?page=' + page)
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          });
      }
    );
  }

  public getProductById(id: number) {
    // In asynchronous requests it's recommended the use of Promises or Observables
    // (in our cases as we have a unique response a Promise will be enough)
    return new Promise(
      resolve => {
        this.http.get('http://multimedia.uoc.edu/frontend/productdetail.php?id=' + id)
          .map(res => res.json())
          .subscribe(data => {
            resolve(data);
          });
      }
    );
  }

}
